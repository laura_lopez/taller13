import os

print("BIENVENIDOS AL JUEGO TRIQUI !!")
print("El tablero se manejara de la siguiente manera:")

print("1|2|3")
print("4|5|6")
print("6|7|8")

        
tablero=["_", "_", "_",
        "_", "_", "_",
        "_", "_", "_"]
        

ganador=None


def jugar():
    global ganador
    ver_tablero()
    
    
    for i in range(5):
        print("turno del jugador1, que sera X")
        valor="X"
        
        jugada(valor)
        hayGanador()
        if ganador!="X" and i<4:
        
            for j in range(3):
                print("turno del jugador2, que sera O")
                valor="O"
                jugada(valor)
                hayGanador()

                if ganador=="O":
                    print("GANADOR jugador 2")
                break
        elif ganador=="X":
            print("GANADOR jugador 1") 
            break
        else:
            print("Empate")       
             
    
def hayGanador():
    global ganador
    TriquiFila()
    TriquiColumna()
    TriquiDiaginal()

def  TriquiFila():
    global ganador
    if tablero[0]==tablero[1]==tablero[2]!="_":
        ganador=tablero[0]
    elif tablero[3]==tablero[4]==tablero[5]!="_":
        ganador=tablero[3]
    elif tablero[6]==tablero[7]==tablero[8]!="_":
        ganador=tablero[6]

def TriquiColumna(): 
    global ganador
    if tablero[0]==tablero[1]==tablero[2]!="_":
        ganador=tablero[0]
    elif tablero[3]==tablero[4]==tablero[5]!="_":
        ganador=tablero[3]
    elif tablero[6]==tablero[7]==tablero[8]!="_":
        ganador=tablero[6]

def TriquiDiaginal():
    global ganador
    if tablero[0]==tablero[4]==tablero[8]!="_":
        ganador=tablero[0]
    elif tablero[2]==tablero[4]==tablero[6]!="_":
        ganador=tablero[3]

    
def jugada(valor):
    anoto=False
    while anoto==False:
        posicion= int(input("Jugador elige una posicion del 1 al 9:"))
        posicion=posicion-1
        
        if tablero[posicion]=="_":
            anoto= True
        else:
            print("Esta posicion se encuentra ocupada")
    tablero[posicion]=valor
    ver_tablero()
            

    
def ver_tablero():
    os.system ("pause")
    os.system ("cls")
    print("EMPECEMOS")
    print("\n")
    print(tablero[0]+"|"+tablero[1]+"|"+tablero[2])
    print(tablero[3]+"|"+tablero[4]+"|"+tablero[5])
    print(tablero[6]+"|"+tablero[7]+"|"+tablero[8])
    print("\n")
    
jugar()